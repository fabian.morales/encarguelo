(function (window, $) {
    var cotizador = {
        cargarForm: function($target) {
            $.ajax({
                url: '/cotizador/light',
                method: 'get',
                success: function(res){
                    $($target).html(res);
                    hookForm();
                }
            });
        }
    };
    
    window.cotizador = cotizador;
    
    function scrollTo(target) {
        var wheight = $(window).height() / 2;

        var alto = 0;//$("#header").hasClass("header_fijo") ? $("#header").height() : $("#header").height() * 2;

        var ooo = $(target).offset().top;
        $('html, body').animate({scrollTop: ooo}, 600);
    }
    
    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
    
    function hookForm(){
        $("#btnAdicionarReg").click(function (e) {
            e.preventDefault();
            var $html = $("#detalle").html();
            var $o = $($.parseHTML($html));
            /*var $cnt = $("#registros > div.row").size();
             $o.attr("id", "registro" + $cnt + Math.floor((Math.random() * 1000) + 1));
             hookRegistro($o, true);*/
            $("#detalles").append($o);
        });
        
        $("#btnEnviarCotizacion").one("click", function(e) {
            e.preventDefault();            
                        
            if ($("#nombre").val() === ''){
                alert('Debe ingresar su nombre');
                $("#nombre").focus();
                return;
            }
            
            if ($("#apellido").val() === ''){
                alert('Debe ingresar su apellido');
                $("#apellido").focus();
                return;
            }
            
            if ($("#email").val() === ''){
                alert('Debe ingresar su dirección de correo');
                $("#email").focus();
                return;
            }
            
            if (!isEmail($("#email").val())){
                alert('La dirección de correo no es válida');
                $("#email").focus();
                return;
            }
            
            var $error = false;
            $("input[rel=nombre_detalle]").each(function(i, o) {
                if ($(o).val() === ''){
                    alert('Debe ingresar todos los nombre de los productos');
                    $error = true;
                    return;
                }
            });
            
            if ($error === true){
                return;
            }
            
            $("input[rel=url_detalle]").each(function(i, o) {
                if ($(o).val() === '' && $("#observaciones").val() === ''){
                    alert('Hay productos sin enlace y tampoco se ha especificado datos en las observaciones');
                    $error = true;
                    return;
                }
            });
            
            if ($error === true){
                return;
            }
            
            $("#loader").addClass("loading");
            $.ajax({
                url: $("#form_cotizacion").attr("action"),
                method: 'post',
                data: $("#form_cotizacion").serialize()
            })
            .done(function(res) {
                $("#loader").removeClass("loading");
                alert('Hemos recibido tu cotización y te la estaremos enviando en máximo 24 horas hábiles. No olvides revisar tu carpeta de no deseados.');
                window.location.href = 'http://encarguelo.com/gracias';
                //window.location.reload();
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                $("#loader").removeClass("loading");
                alert(jqXHR.responseJSON.error.message);
            });
        });
        
        $("#telefono").keypress(function(e) {
            if (e.which < 48 || e.which > 57){
                e.preventDefault();
            }
        });
    }
})(window, jQuery);