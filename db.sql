create table sis_par_usuario(
    id int auto_increment primary key,
    nombre varchar(200),
    login varchar(100),
    email varchar(200),
    password varchar(100),
    admin char(1) default 'N',
    activo char(1) default 'Y',
    alerta_creacion char(1) default 'N',
    alerta_edicion char(1) default 'N',
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    remember_token timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_par_cliente(
    id int auto_increment primary key,
    nombres varchar(200),
    apellidos varchar(200),
    telefono varchar(100),
    email varchar(100),
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_ped_encabezado(
    id int auto_increment primary key,
    id_cliente int,
    id_usuario int,
    fecha_creacion timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    fecha_cotizacion timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    fecha_pago timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    fecha_envio timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    estado enum('N', 'C', 'P', 'E') default 'N',
    valor numeric(10, 2) default 0,
    observaciones text,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_cliente) references sis_par_cliente (id),
    foreign key (id_usuario) references sis_par_usuario (id)
);

create table sis_ped_detalle(
    id int auto_increment primary key,
    id_pedido int,
    nombre varchar(200),
    link text,
    valor numeric(10, 2) default 0,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_pedido) references sis_ped_encabezado (id)
);

create table sis_ped_token(
    id int auto_increment primary key,
    id_pedido int,
    token varchar(20),
    fecha_vencimiento date,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_pedido) references sis_ped_encabezado (id)
);

create table sis_ped_intento_pago(
    id int auto_increment primary key,
    id_pedido int,
    id_token int,
    fecha timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    fecha_transaccion timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    respuesta varchar(50),
    franquicia varchar(200),
    num_transaccion varchar(20),
    cod_aprobacion varchar(20),
    ref_payco varchar(50),
    mensaje text,
    estado enum('P', 'A', 'R', 'V') default 'P',
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_pedido) references sis_ped_encabezado (id)
);