<?php

namespace App;

class Pedido extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'ped_encabezado';
    
    public function detalles(){
        return $this->hasMany('\App\Detalle', 'id_pedido');
    }
    
    public function cliente(){
        return $this->hasOne('\App\Cliente', 'id', 'id_cliente');
    }
    
    public function tokens(){
        return $this->hasMany('\App\TokenPedido', 'id_pedido');
    }
    
    public function intentosPago(){
        return $this->hasMany('\App\IntentoPago', 'id_pedido');
    }
}
