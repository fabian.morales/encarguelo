<?php

namespace App;

class TokenPedido extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'ped_token';
    
    public function pedido(){
        return $this->belongsTo('\App\Pedido', 'id_pedido', 'id');
    }
    
    public function intentoPago(){
        return $this->hasMany('\App\IntentoPago', 'id_token');
    }
}
