<?php

namespace App;

class Detalle extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'ped_detalle';
    
    public function pedido(){
        return $this->belongsTo('\App\Pedido', 'id_pedido', 'id');
    }
    
    public static function addhttp($url) {
        if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
            $url = "http://" . $url;
        }
        
        return $url;
    }
}
