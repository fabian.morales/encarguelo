<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SondaPago extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sondear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verifica el estado de los pedidos pendientes';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->comment(PHP_EOL . Inspiring::quote() . PHP_EOL);
    }

}
