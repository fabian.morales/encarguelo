<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

class AdminController extends BaseController {
    //private $validarPermiso = false;
    
    function __construct() {
        /*$this->beforeFilter(function() {
            if (!\Auth::check()){
                return \Redirect::to("/sesion/formLogin")->with("mensajeError", "Necesita estar logueado para ingresar a esta secci&oacute;n");
            }
            
            $usuario = Auth::user();
            list($clase, $accion) = explode("@", Route::currentRouteAction());
            
            $controlador = Controlador::where("nombre_clase", $clase)->first();
            if (sizeof($controlador) && $controlador->validar_permiso == "Y"){
                $usuario = Auth::user();
                
                $c_permiso = $usuario->controladores()->where("nombre_clase", $clase)->count();
                if ($usuario->admin != "Y" && $c_permiso == 0){
                    return Redirect::to("/")->with("mensajeError", "No tiene permisos para ingresar a esta secci&oacute;n");
                }
            }
        });*/
        $this->middleware('auth');
    }
    
    public function mostrarIndex(){
        return \View::make("index");
    }
}