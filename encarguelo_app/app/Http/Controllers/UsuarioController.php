<?php

namespace App\Http\Controllers;

use \Illuminate\Support\Facades\Input;

class UsuarioController extends AdminController {

    public function mostrarIndex() {
        $usuarios = \App\User::paginate(20);
        return \View::make('usuario.index', array("usuarios" => $usuarios));
    }

    public function mostrarFormUsuario($usuario) {
        if (!sizeof($usuario)) {
            $usuario = new \App\User();
        }

        return \View::make("usuario.form", array("usuario" => $usuario));
    }

    public function crearUsuario() {
        return $this->mostrarFormUsuario(new \App\User());
    }

    public function editarUsuario($id) {
        $usuario = \App\User::find($id);
        if (!sizeof($usuario)) {
            return \Redirect::action('UsuarioController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el usuario");
        }

        return $this->mostrarFormUsuario($usuario);
    }

    public function guardarUsuario() {
        $id = Input::get("id");
        $clave = Input::get("password");

        $usuario = \App\User::find($id);
        if (!sizeof($usuario)) {
            $usuario = new \App\User();
        }

        if (empty($id) && empty($clave)) {
            \Illuminate\Support\Facades\Session::flash("mensajeError", "Debe ingresar la clave para el usuario");
            return $this->mostrarFormUsuario($usuario);
        } else if (!empty($clave)) {
            $clave = \Illuminate\Support\Facades\Hash::make($clave);
        } else {
            $clave = $usuario->password;
        }

        $usuario->fill(Input::all());
        $usuario->password = $clave;

        if (!empty($usuario->id)) {
            $cntLogin = \App\User::where("login", $usuario->login)->where("id", "<>", $usuario->id)->count();
            if ($cntLogin > 0) {
                \Illuminate\Support\Facades\Session::flash("mensajeError", "Ya existe un usuario con el login ingresado");
                return $this->mostrarFormUsuario($usuario);
            }
        }

        if ($usuario->save()) {
            return \Redirect::action('UsuarioController@mostrarIndex')->with("mensaje", "Usuario guardado exitosamente");
        } else {
            return \Redirect::action('UsuarioController@mostrarIndex')->with("mensajeError", "No se pudo guardar el usuario");
        }
    }

}
