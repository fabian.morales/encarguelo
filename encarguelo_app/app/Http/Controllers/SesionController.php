<?php

namespace App\Http\Controllers;

use \Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;

class SesionController extends Controller {

    public function mostrarIndex(){
        if (\Auth::check()){
            return \Redirect::action("AdminController@mostrarIndex");
        }
        return \View::make('sesion.login');
    }
       
    public function hacerLogin(){
        $login = Input::get("login");
        $clave = Input::get("password");
        if (\Auth::attempt(array('login' => $login, 'password' => $clave))){
            return \Redirect::action("AdminController@mostrarIndex")->with("mensaje", "Logueado");
        }
        else{
            return \Redirect::action("SesionController@mostrarIndex")->with("mensajeError", "Usuario o clave incorrecta");
        }
    }
    
    public function hacerLogout(){
        \Auth::logout();
        return \Redirect::action("SesionController@mostrarIndex");
    }
}