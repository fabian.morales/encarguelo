<?php

namespace App\Http\Controllers;

use \Illuminate\Support\Facades\Input;
use \Illuminate\Support\Facades\Mail;

class CotizadorController extends Controller {

    public function mostrarIndex() {
        return \View::make('cotizador.form');
    }
    
    public function mostrarFormLight() {
        return \View::make('cotizador.form_light');
    }

    public function enviarCotizacion() {

        date_default_timezone_set('America/Bogota');
        \Illuminate\Support\Facades\DB::beginTransaction();
        
        try {

            $email = Input::get("email");
            $cliente = \App\Cliente::where("email", $email)->first();
            if (!sizeof($cliente)) {
                $cliente = new \App\Cliente();
            }

            $cliente->nombres = Input::get("nombre");
            $cliente->apellidos = Input::get("apellido");
            $cliente->telefono = Input::get("telefono");
            $cliente->email = $email;

            if (empty($cliente->nombres)) {
                throw new \Exception('Debe ingresar su nombre');
            }
            
            if (empty($cliente->apellidos)) {
                throw new \Exception('Debe ingresar su apellido');
            }

            if (empty($cliente->email)) {
                throw new \Exception('Debe ingresar su dirección de correo');
            }

            if (!$cliente->save()) {
                throw new \Exception('No se pudo guardar los datos de cliente');
            }

            $pedido = new \App\Pedido();
            $pedido->fecha_creacion = date('Y-m-d H:i:s');
            $pedido->id_cliente = $cliente->id;
            $pedido->observaciones = Input::get("observaciones");
            $pedido->estado = 'N';
            $pedido->valor = 0;

            if (!$pedido->save()) {
                throw new \Exception('No se pudo guardar el pedido');
            }

            $nombresDetalle = Input::get("nombre_detalle");
            $urlsDetalle = Input::get("url_detalle");
            $detalles = [];

            foreach ($nombresDetalle as $i => $nombre) {
                if (empty($nombre)) {
                    throw new \Exception('Debe ingresarse el nombre de todos los productos');
                }
                
                if (empty($urlsDetalle[$i]) && empty($pedido->observaciones)) {
                    throw new \Exception('Hay productos sin enlace y tampoco se ha especificado datos en las observaciones');
                }
                
                $detalle = new \App\Detalle();
                $detalle->id_pedido = $pedido->id;
                $detalle->nombre = $nombre;
                $detalle->link = $urlsDetalle[$i];
                $detalle->valor = 0;

                if (!$detalle->save()) {
                    throw new \Exception('No se pudo guardar el detalle del pedido');
                }

                $detalles[] = $detalle;
                
            }

            if (!sizeof($detalles)) {
                throw new \Exception('No se ingresaron detalles al pedido');
            }

            \Illuminate\Support\Facades\DB::commit();

            $datosEmail = [
                "cliente" => $cliente,
                "pedido" => $pedido,
                "detalles" => $detalles
            ];

            Mail::send('emails.cotizador.pedido_cliente', $datosEmail, function($message) use ($cliente) {
                $message->from('info@encarguelo.com', 'Encarguelo.com');
                $message->subject('Hemos recibido tu solicitud');
                $message->to($cliente->email, $cliente->nombres);
                $message->bcc('desarrollo@encubo.ws');
            });

            Mail::send('emails.cotizador.pedido_admin', $datosEmail, function($message) {
                $message->from('info@encarguelo.com', 'Encarguelo.com');
                $message->subject('Solicitud de pedido recibida');
                $message->to('info@encarguelo.com');
                $message->bcc('desarrollo@encubo.ws');
                //$message->attach($pathToFile);
            });

            \Illuminate\Support\Facades\Session::flash("mensaje", "Su solicitud se ha enviado exitosamente");
            return \Redirect::action('CotizadorController@mostrarIndex');
        } catch (\Exception $e) {
            \Illuminate\Support\Facades\DB::rollback();
            return $this->retornarError($e);
        }
    }

    public function pagarPedido($id) {
        date_default_timezone_set('America/Bogota');
        $token = \App\TokenPedido::where('token', $id)->with(['pedido.detalles', 'pedido.cliente'])->first();

        if (!sizeof($token)) {
            return \Redirect::action('CotizadorController@mostrarIndex')->with('mensajeError', 'Token no válido');
        }

        $cntIntentos = \App\IntentoPago::where('id_token', $token->id)->where('estado', 'A')->count();
        if ($cntIntentos > 0) {
            return \Redirect::action('CotizadorController@mostrarIndex')->with('mensajeAviso', 'Este pedido ya ha sido pagado');
        }

        $cntIntentos = \App\IntentoPago::where('id_token', $token->id)->where('estado', 'V')->count();
        if ($cntIntentos > 0) {
            return \Redirect::action('CotizadorController@mostrarIndex')->with('mensajeAviso', 'Este pedido tiene un proceso de verificación de pago pendiente');
        }

        $intento = \App\IntentoPago::where('id_token', $token->id)->where('estado', 'P')->first();

        if (!sizeof($intento)) {
            $intento = new \App\IntentoPago();
            $intento->id_pedido = $token->id_pedido;
            $intento->id_token = $token->id;
            $intento->fecha = date('Y-m-d H:i:s');
            $intento->estado = 'P';

            if (!$intento->save()) {
                return \Redirect::action('CotizadorController@mostrarIndex')->with('mensajeError', 'No se pudo registrar los datos de pago. Intente nuevamente.');
            }
        }

        $datos = [
            "pedido" => $token->pedido,
            "token" => $token,
            "intento" => $intento,
            "clienteId" => '9723',
            "key" => '7dee729a0911068fa530e92171d54f5b2515b96e',
            "firma" => sha1('7dee729a0911068fa530e92171d54f5b2515b96e' . '9723')
        ];

        return \View::make('cotizador.pago', $datos);
    }

    public function respuestaPago() {

        try {
            \Illuminate\Support\Facades\DB::beginTransaction();

            $token = \App\TokenPedido::where('token', Input::get("x_extra1"))->first();
            if (!sizeof($token)) {
                return \Redirect::action('CotizadorController@mostrarIndex')->with('mensajeError', 'Token no válido');
            }

            $intento = \App\IntentoPago::find(Input::get("x_extra2"));
            if (!sizeof($intento)) {
                return \Redirect::action('CotizadorController@mostrarIndex')->with('mensajeError', 'Intento de pago no válido');
            }

            if ($token->id != $intento->id_token) {
                return \Redirect::action('CotizadorController@mostrarIndex')->with('mensajeError', 'El intento de pago no corresponde con el token');
            }

            $medioPago = Input::get('x_franchise');
            $franquicia = "";

            switch ($medioPago) {
                case 'VS':
                    $franquicia = "Visa";
                    break;
                case 'MC':
                    $franquicia = "MasterCard";
                    break;
                case 'AM':
                    $franquicia = "American Express";
                    break;
                case 'DN':
                    $franquicia = "Diners";
                    break;
                case 'CR':
                    $franquicia = "Credencial";
                    break;
                case 'PSE':
                    $franquicia = "PSE (Proveedor de Servicios Electr&oacute;nicos)";
                    break;
                case 'DV':
                    $franquicia = "Debito Visa";
                    break;
                case 'DM':
                    $franquicia = "Debito MasterCard";
                    break;
                default:
                    $franquicia = $medioPago;
            }

            $pedido = \App\Pedido::find($token->id_pedido);
            if (!sizeof($pedido)) {
                return \Redirect::action('CotizadorController@mostrarIndex')->with('mensajeError', 'Pedido no encontrado');
            }

            $intento->fecha_transaccion = Input::get('x_fecha_transaccion');
            $intento->respuesta = Input::get('x_respuesta');
            $intento->franquicia = $franquicia;
            $intento->num_transaccion = Input::get('x_transaction_id');
            $intento->cod_aprobacion = Input::get('x_approval_code');
            $intento->ref_payco = Input::get('x_ref_payco');
            $intento->mensaje = Input::get('x_response_reason_text');

            $clase = "";

            switch ($intento->respuesta) {
                case 'Aceptada':
                    $intento->estado = 'A';
                    $pedido->estado = 'P';
                    $clase = "texto verde";
                    break;
                case 'Rechazada':
                    $intento->estado = 'R';
                    $clase = "texto rojo";
                    break;
                case 'Pendiente':
                    $intento->estado = 'V';
                    $clase = "texto amarillo";
                    break;
            }

            if (!$pedido->save()) {
                throw new \Exception('No se pudo actualizar el pedido');
            }

            if (!$intento->save()) {
                throw new \Exception('No se pudo registrar los datos de pago');
            }

            $imagen = public_path('imagenes/franquicias/' . strtolower($medioPago) . '.png');
            $imagenFranq = is_file($imagen) ? strtolower($medioPago) : "default";

            \Illuminate\Support\Facades\DB::commit();

            return \View::make('cotizador.respuesta', ["intento" => $intento, "pedido" => $pedido, "franquicia" => $imagenFranq, "claseRespuesta" => $clase]);
        } catch (\Exception $e) {
            \Illuminate\Support\Facades\DB::rollback();
            return \Redirect::action('CotizadorController@mostrarIndex')->with('mensajeError', $e->getMessage());
        }
    }

}
