<?php

namespace App\Http\Controllers;

use \Illuminate\Support\Facades\Input;
use \Illuminate\Support\Facades\Mail;

class PedidoController extends AdminController {

    public function mostrarIndex() {
        $pedidos = \App\Pedido::with('cliente')->orderBy('fecha_creacion', 'desc');
        $estados = [
            "N" => "Nuevo",
            "C" => "Cotizado",
            "P" => "Pagado",
            "E" => "Enviado"
        ];
        
        $fechaInicio = Input::get("fecha_inicio");
        $fechaFin = Input::get("fecha_fin");
        $cliente = Input::get("cliente");
        
        if (!empty($fechaInicio)){
            $pedidos = $pedidos->where("fecha_creacion", ">=", $fechaInicio." 00:00:00");
        }
        
        if (!empty($fechaFin)){
            $pedidos = $pedidos->where("fecha_creacion", "<=", $fechaFin." 23:59:59");
        }
        
        if (!empty($cliente)){
            $pedidos = $pedidos->whereHas('cliente', function($q) use ($cliente) {
                $q->where("nombres", "like", "%".$cliente."%")->orWhere("apellidos", "like", "%".$cliente."%");
            });
        }
        
        $pedidos = $pedidos->paginate(40);
        
        return \View::make('pedido.index', array("pedidos" => $pedidos, "estados" => $estados, "fecha_inicio" => $fechaInicio, "fecha_fin" => $fechaFin, "cliente" => $cliente));
    }

    public function editarPedido($id) {
        $pedido = \App\Pedido::where('id', $id)->with(['cliente', 'detalles'])->first();
        if (!sizeof($pedido)) {
            return \Redirect::action('PedidoController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el pedido");
        }

        return \View::make("pedido.form", array("pedido" => $pedido));
    }

    public function mostrarIntentosPago($id) {
        $pedido = \App\Pedido::where("id", $id)->with(['intentosPago.token'])->first();
        if (!sizeof($pedido)) {
            return \Redirect::action('PedidoController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el pedido");
        }
        $estados = ["P" => "Pendiente", "A" => "Aprobado", "R" => "Rechazado", "V" => "En verificación"];
        return \View::make("pedido.pagos", array("pedido" => $pedido, "estados" => $estados));
    }

    public function guardarPedido() {
        $id = Input::get("id");

        $pedido = \App\Pedido::where("id", $id)->with(['cliente', 'detalles'])->first();
        if (!sizeof($pedido)) {
            return \Redirect::action('PedidoController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el pedido");
        }

        if ($pedido->estado != 'N' && $pedido->estado != 'C') {
            return \Redirect::action('PedidoController@mostrarIndex')->with("mensajeError", "No se puede editar este pedido");
        }

        $valores = Input::get("detalle");
        $valorTotalDet = 0;
        foreach ($pedido->detalles as $d) {
            $valorTotalDet += (float) $valores[$d->id];
            $d->valor = (float) $valores[$d->id];
            $d->save();
        }

        $pedido->valor = $valorTotalDet;
        $pedido->fecha_cotizacion = date('Y-m-d H:i:s');
        $pedido->id_usuario = \Auth::user()->id;
        $pedido->estado = 'C';

        if ($pedido->save()) {
            $token = \App\TokenPedido::where("id_pedido", $pedido->id)->first();

            if (!sizeof($token)) {
                $token = new \App\TokenPedido();
                $token->id_pedido = $pedido->id;
                $token->token = str_random(20);
                $token->save();
            }

            Mail::send('emails.pedido.cotizacion', ["pedido" => $pedido, "token" => $token], function($message) use($pedido) {
                $message->from('info@encarguelo.com', 'Encarguelo.com');
                $message->subject('Encargo cotizado');
                $message->to($pedido->cliente->email, $pedido->cliente->nombres);
                $message->bcc('desarrollo@encubo.ws');
            });

            return \Redirect::action('PedidoController@mostrarIndex')->with("mensaje", "Pedido actualizado exitosamente");
        } else {
            return \Redirect::action('PedidoController@mostrarIndex')->with("mensajeError", "No se pudo actualizar el pedido");
        }
    }

    function enviarCorreo($id) {
        $pedido = \App\Pedido::where("id", $id)->with(['cliente', 'detalles'])->first();
        if (!sizeof($pedido)) {
            return \Redirect::action('PedidoController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el pedido");
        }

        if ($pedido->estado != 'C') {
            return \Redirect::action('PedidoController@mostrarIndex')->with("mensajeError", "El pedido debe ser cotizado primero");
        }

        $token = \App\TokenPedido::where("id_pedido", $pedido->id)->first();

        if (!sizeof($token)) {
            $token = new \App\TokenPedido();
            $token->id_pedido = $pedido->id;
            $token->token = str_random(20);
            $token->save();
        }

        Mail::send('emails.pedido.cotizacion', ["pedido" => $pedido, "token" => $token], function($message) use($pedido) {
            $message->from('info@encarguelo.com', 'Encarguelo.com');
            $message->subject('Encargo cotizado');
            $message->to($pedido->cliente->email, $pedido->cliente->nombres);
            $message->bcc('desarrollo@encubo.ws');
        });

        return \Redirect::action('PedidoController@mostrarIndex')->with("mensaje", "Correo enviado al cliente");
    }

    function enviarPedido($id) {
        $pedido = \App\Pedido::where("id", $id)->with(['cliente', 'detalles'])->first();
        if (!sizeof($pedido)) {
            return \Redirect::action('PedidoController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el pedido");
        }

        if ($pedido->estado != 'P') {
            return \Redirect::action('PedidoController@mostrarIndex')->with("mensajeError", "El cliente no ha pagado este pedido");
        }

        $pedido->fecha_cotizacion = date('Y-m-d H:i:s');
        $pedido->id_usuario = \Auth::user()->id;
        $pedido->estado = 'E';

        if ($pedido->save()) {
            Mail::send('emails.pedido.envio', ["pedido" => $pedido], function($message) use($pedido) {
                $message->from('info@encarguelo.com', 'Encarguelo.com');
                $message->subject('Encargo enviado');
                $message->to($pedido->cliente->email, $pedido->cliente->nombres);
                $message->bcc('desarrollo@encubo.ws');
            });

            return \Redirect::action('PedidoController@mostrarIndex')->with("mensaje", "Pedido marcado como enviado");
        } else {
            return \Redirect::action('PedidoController@mostrarIndex')->with("mensajeError", "No se pudo marcar como enviado el pedido");
        }
    }
    
    function marcarPagado($id) {
        $pedido = \App\Pedido::where("id", $id)->first();
        if (!sizeof($pedido)) {
            return \Redirect::action('PedidoController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el pedido");
        }

        if ($pedido->estado != 'C') {
            return \Redirect::action('PedidoController@mostrarIndex')->with("mensajeError", "El pedido no ha sido cotizado");
        }
        
        $pedido->estado = 'P';

        if ($pedido->save()) {
            return \Redirect::action('PedidoController@mostrarIndex')->with("mensaje", "Pedido marcado como pagado");
        } else {
            return \Redirect::action('PedidoController@mostrarIndex')->with("mensajeError", "No se pudo marcar como pagado el pedido");
        }
    }
    
    function exportarCsv() {
        $clientes = \App\Cliente::all();
        $contenido = "Nombre,Apellido,Telefono,Email\n";
        
        foreach ($clientes as $c){
            $contenido .= $c->nombres.",".$c->apellidos.",".$c->telefono.",".$c->email."\n";
        }
        
        $file = tempnam(sys_get_temp_dir(), "Clientes_");
        file_put_contents($file, $contenido);
        
        $headers = array(
            'Content-Type: application/csv',
        );
        return response()->download($file, 'clientes.csv', $headers);
    }
    
    function borrarPedido($id){
        $pedido = \App\Pedido::find($id);
        if (!sizeof($pedido)) {
            return \Redirect::action('PedidoController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el pedido");
        }
        
        $pedido->detalles()->delete();
        $pedido->intentosPago()->delete();
        $pedido->tokens()->delete();
        
        if ($pedido->delete()) {
            return \Redirect::back()->with("mensaje", "Pedido borrado exitosamente");
        } else {
            return \Redirect::back()->with("mensajeError", "No se pudo borrar el pedido");
        }
    }

}
