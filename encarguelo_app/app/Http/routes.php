<?php

/*
  |--------------------------------------------------------------------------
  | Routes File
  |--------------------------------------------------------------------------
  |
  | Here is where you will register all of the routes in an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */
Route::get('/crearAdmin', function() {
    $usuario = new App\User();
    $usuario->nombre = "Admin";
    $usuario->login = "admin";
    $usuario->password = \Hash::make("Ev4nerv");
    $usuario->admin = "Y";
    $usuario->save();
});

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | This route group applies the "web" middleware group to every route
  | it contains. The "web" middleware group is defined in your HTTP
  | kernel and includes session state, CSRF protection, and more.
  |
 */

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'CotizadorController@mostrarIndex');
    Route::get('/light', 'CotizadorController@mostrarFormLight');
    Route::post('/cotizacion/enviar', 'CotizadorController@enviarCotizacion');
    Route::get('/cotizacion/pagar/{id}', 'CotizadorController@pagarPedido');
    Route::post('/cotizacion/respuesta', 'CotizadorController@respuestaPago');

    Route::group(array('prefix' => 'administrador'), function() {
        Route::get('/', 'AdminController@mostrarIndex');
        Route::get('/login', 'SesionController@mostrarIndex');
        Route::post('/login', 'SesionController@hacerLogin');
        Route::get('/logout', 'SesionController@hacerLogout');

        Route::group(array('prefix' => 'usuarios'), function() {
            Route::get('/', 'UsuarioController@mostrarIndex');
            Route::get('/crear', 'UsuarioController@crearUsuario');
            Route::get('/editar/{id}', 'UsuarioController@editarUsuario');
            Route::post('/guardar', 'UsuarioController@guardarUsuario');
        });

        Route::group(array('prefix' => 'pedidos'), function() {
            Route::any('/', 'PedidoController@mostrarIndex');
            Route::get('/editar/{id}', 'PedidoController@editarPedido');
            Route::get('/pagos/{id}', 'PedidoController@mostrarIntentosPago');
            Route::post('/guardar', 'PedidoController@guardarPedido');
            Route::get('/mail/{id}', 'PedidoController@enviarCorreo');
            Route::get('/enviar/{id}', 'PedidoController@enviarPedido');
            Route::get('/pagar/{id}', 'PedidoController@marcarPagado');
            Route::get('/borrar/{id}', 'PedidoController@borrarPedido');
            Route::get('/clientes', 'PedidoController@exportarCsv');
        });
    });
    
    Route::get('/bleh', function() {
        $p1 = \App\Pedido::find(317);
        $p2 = \App\Pedido::find(319);
        
        echo $p2->fecha_creacion." - ".$p1->fecha_creacion."<br />";
        $p = strtotime($p2->fecha_creacion) - strtotime($p1->fecha_creacion);
        echo $p;
    });
});
