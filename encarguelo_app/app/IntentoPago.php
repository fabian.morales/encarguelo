<?php

namespace App;

class IntentoPago extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'ped_intento_pago';
    
    public function pedido(){
        return $this->belongsTo('\App\Pedido', 'id_pedido', 'id');
    }
    
    public function token(){
        return $this->belongsTo('\App\TokenPedido', 'id_token', 'id');
    }
}
