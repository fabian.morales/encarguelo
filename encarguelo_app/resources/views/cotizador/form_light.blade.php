<form id="form_cotizacion" method="post" action="{{ url('/cotizacion/enviar') }}">
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <div class="row">
        <div class="medium-12 columns">
            <div class="row">
                <div class="small-12 columns">
                    <h3 class="titulo seccion"><span>Cotiza tu encargo</span></h3>
                    <p>Llena el formulario con el mayor detalle posible y tu cotizaci&oacute;n estar&aacute; lista en m&aacute;ximo 24 horas</p>
                </div>
                <div class="large-2 small-4 columns"><label for="nombre">Nombre<sup>*</sup></label></div>
                <div class="large-4 small-8 columns"><input type="text" id="nombre" name="nombre" requiered /></div>
                <div class="large-2 small-4 columns"><label for="apellido">Apellidos<sup>*</sup></label></div>
                <div class="large-4 small-8 columns"><input type="text" id="apellido" name="apellido" requiered /></div>
                
                <div class="large-2 small-4 columns"><label for="email">Correo<sup>*</sup></label></div>
                <div class="large-4 small-8 columns"><input type="email" id="email" name="email" required /></div>
                <div class="large-2 small-4 columns"><label for="nombre">Tel&eacute;fono</label></div>
                <div class="large-4 small-8 columns"><input type="text" id="telefono" name="telefono"/></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="large-12 columns">
            <div class="row">
                <div class="small-12 columns">
                    <h3 class="titulo seccion"><span>A&ntilde;ade los productos que deseas cotizar</span></h3>
                </div>
            </div>
            <div id="detalle">
                <div class="row">
                    <div class="large-5 columns">
                        <label>Producto</label>
                        <input type="text" name="nombre_detalle[]" rel="nombre_detalle" required />
                    </div>
                    <div class="large-7 columns">
                        <label>Enlace (URL)</label>
                        <input type="text" name="url_detalle[]" rel="url_detalle" />
                    </div>
                </div>
            </div>
            <div id="detalles"></div>
            <div class="row">
                <div class="small-12 columns">
                    <input type="button" id="btnAdicionarReg" class="tiny button default float-right" value="A&ntilde;adir otro producto" />
                </div>
            </div>
        </div>
        <div class="small-12 columns"><hr /></div>
        <div class="small-12 columns"><label for="obsrvaciones">Especificaciones del producto (opcional)</label></div>
        <div class="small-12 columns"><textarea id="observaciones" name="observaciones" rows="5" placeholder="Talla, color, modelo, características del producto, etc."></textarea></div>
        <div class="small-12 columns"><hr /></div>
        <div class="small-12 columns"><input type="submit" id="btnEnviarCotizacion" class="button alert float-right" value="Enviar cotizaci&oacute;n" /></div>
    </div>
</form>