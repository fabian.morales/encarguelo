@extends('master')

@section('content')
<div class="row">
    <div class="small-12 columns">
        <h3 class="titulo seccion"><span>Datos del pedido</span></h3>
    </div>
</div>

<div class="row">
    <div class="medium-2 columns"><strong>Pedido:</strong></div>
    <div class="medium-10 columns">{{ $pedido->id }}</div>

    <div class="medium-2 columns"><strong>Fecha:</strong></div>
    <div class="medium-10 columns">{{ $pedido->fecha_creacion }}</div>

    <div class="medium-2 columns"><strong>Valor total:</strong></div>
    <div class="medium-10 columns">$ {{ $pedido->valor }}</div>
</div>

<div class="row separador">
    <div class="small-12 columns">
        <h3 class="titulo seccion"><span>Detalles</span></h3>
    </div>
</div>

<div class="row item lista head">
    <div class="small-4 columns"><strong>Nombre</strong></div>
    <div class="small-3 columns"><strong>Url</strong></div>
    <div class="small-5 columns"><strong>Valor</strong></div>
</div>
@foreach($pedido->detalles as $d)
<div class="row item lista">
    <div class="small-4 columns">{{ $d->nombre }}</div>
    <div class="small-3 columns">@if(!empty($d->link))<a href="{{ \App\Detalle::addhttp($d->link) }}" target="_blank"><i class="fi-web"></i> Ver</a>@else &nbsp; @endif</div>
    <div class="small-5 columns">$ {{ $d->valor }}</div>
</div>
@endforeach
<div class="row separador"></div>
<div class="row">
    <div class="medium-2 columns"><strong>Observaciones:</strong></div>
    <div class="medium-10 columns">{{ $pedido->observaciones }}</div>
</div>
<div class="row separador"></div>
<form action="https://secure.payco.co/payment.php" method="post" enctype="application/x-www-form-urlencoded" name="paymentForm" id="paymentForm">
    <input type="hidden" name="p_cust_id_cliente" type="text" value="{{ $clienteId }}" />
    <input type="hidden" name="p_key" type="text" value="{{ $firma }}" />
    <input type="hidden" name="p_id_factura" type="text" value="{{ $pedido->id }}" />
    <input type="hidden" name="p_currency_code" type="text" value="COP" />
    <input type="hidden" name="p_amount" type="text" value="{{ $pedido->valor }}" />
    <input type="hidden" name="p_description" type="text" value="ORDEN DE COMPRA # {{ $pedido->id }}" />
    <input type="hidden" name="p_email" type="text" value="{{ $pedido->cliente->email }}" />
    <input type="hidden" name="p_url_respuesta" type="text" value="{{ url('/cotizacion/respuesta') }}" />
    <input type="hidden" name="p_url_confirmacion" type="text" value="{{ url('/cotizacion/respuesta') }}" />
    <input type="hidden" name="p_tax" type="text" value="0" />
    <input type="hidden" name="p_amount_base" type="text" value="0" />
    <input type="hidden" name="p_test_request" type="text" value="FALSE" />
    <input type="hidden" name="p_extra1" type="text" value="{{ $token->token }}" />
    <input type="hidden" name="p_extra2" type="text" value="{{ $intento->id }}" />
    <div class="row">
        <div class="small-12 columns"><input type="submit" id="btnPagar" class="button alert float-right" value="Realizar pago" /></div>
    </div>
</form>
@stop