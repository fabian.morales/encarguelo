@extends('master')

@section('content')
<div class="row">
    <div class="small-12 columns">
        <h3 class="titulo seccion"><span>Resultado de la trasacción</span></h3>
    </div>
</div>

<div class="row">
    <div class="small-7 columns small-centered">
        <table>
            <tbody>
                <tr>
                    <td colspan="3" class="text-center">
            <big><strong class="{{ $claseRespuesta }}">Transacci&oacute;n {{ $intento->respuesta }}</strong></big>
            </td>
            </tr>
            <tr>
                <td colspan="3" class="text-center">
            <big><strong>Valor transacci&oacute;n: $ {{ $pedido->valor }}</strong></big>
            </td>
            </tr>
            <tr>
                <td rowspan="4"><img src="{{ asset('/imagenes/franquicias/'.$franquicia.'.png') }}" /></td>
                <td><strong>Franquicia</strong></td>
                <td>{{ $intento->franquicia }}</td>
            </tr>
            <tr>
                <td><strong>Fecha transacci&oacute;n</strong></td>
                <td>{{ $intento->fecha_transaccion }}</td>
            </tr>
            <tr>
                <td><strong>N&uacute;mero de transacci&oacute;n</strong></td>
                <td>{{ $intento->num_transaccion }}</td>
            </tr>
            <tr>
                <td><strong>C&oacute;digo de aprobaci&oacute;n</strong></td>
                <td>{{ $intento->cod_aprobacion }}</td>
            </tr>
            <tr>
                <td class="text-right"><strong>Mensaje</strong></td>
                <td>{{ $intento->mensaje }}</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

@stop