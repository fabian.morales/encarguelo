<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Encarguelo</title>
        <link rel='stylesheet' id='flatsome-googlefonts-css'  href='//fonts.googleapis.com/css?family=Dancing+Script%3A300%2C400%2C700%2C900%7CLato%3A300%2C400%2C700%2C900%7CLato%3A300%2C400%2C700%2C900%7CLato%3A300%2C400%2C700%2C900&#038;subset=latin&#038;ver=4.4.2' type='text/css' media='all' />
        <link href="{{ asset('foundation/css/foundation.css') }}" rel="stylesheet" />
        <link href="{{ asset('foundation/css/app.css') }}" rel="stylesheet" />
        <link href="{{ asset('foundation/motion-ui/motion-ui.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('foundation/fonts/foundation-icons.css') }}" rel="stylesheet" />
        <link href="{{ asset('css/estilos.css') }}" rel="stylesheet" />
        <link rel="icon" href="{{ asset('/imagenes/favicon.png') }}" sizes="32x32" />

        <script src="{{ asset('js/jquery-1.12.1.min.js') }}"></script>
        <script src="{{ asset('foundation/js/foundation.min.js') }}"></script>
        <script src="{{ asset('foundation/js/app.js') }}"></script>
        <script src="{{ asset('foundation/motion-ui/motion-ui.min.js') }}"></script>
        <script src="{{ asset('js/script.js') }}" language="javascript"></script>
        <script>
        $(document).foundation();
        (function (window, $) {
            $(document).ready(function () {
                $(document).foundation();
            });
        })(window, jQuery);
        </script>
    </head>
    <body>
        @include('cotizador.form_light')
    </body>
</html>
