<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Encarguelo</title>
        <link rel='stylesheet' id='flatsome-googlefonts-css'  href='//fonts.googleapis.com/css?family=Dancing+Script%3A300%2C400%2C700%2C900%7CLato%3A300%2C400%2C700%2C900%7CLato%3A300%2C400%2C700%2C900%7CLato%3A300%2C400%2C700%2C900&#038;subset=latin&#038;ver=4.4.2' type='text/css' media='all' />
        <link href="{{ asset('foundation/css/foundation.css') }}" rel="stylesheet" />
        <link href="{{ asset('foundation/css/app.css') }}" rel="stylesheet" />
        <link href="{{ asset('foundation/motion-ui/motion-ui.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('foundation/fonts/foundation-icons.css') }}" rel="stylesheet" />
        <link href="{{ asset('css/estilos.css') }}" rel="stylesheet" />
        <link href="{{ asset('css/tema.css') }}" rel="stylesheet" />
        <link rel="icon" href="{{ asset('/imagenes/favicon.png') }}" sizes="32x32" />
        @section ('css_header')
        @show

        <script src="{{ asset('js/jquery-1.12.1.min.js') }}"></script>
        <script src="{{ asset('foundation/js/foundation.min.js') }}"></script>
        <script src="{{ asset('foundation/js/app.js') }}"></script>
        <script src="{{ asset('foundation/motion-ui/motion-ui.min.js') }}"></script>
        <script src="{{ asset('js/script.js') }}" language="javascript"></script>
        <script>
        $(document).foundation();
        (function (window, $) {
            $(document).ready(function () {
                $(document).foundation();
            });
        })(window, jQuery);
        </script>
        @section ('js_header')
        @show
    </head>
    <body>
        <div id="loader"><div></div></div>
        <header>
            <section id="top-bar">
                <div class="row">
                    <div class="small-12 columns">
                        <div>
                            Encarguelo.com se dedica a la compra de cualquier art&iacute;culo de origen extranjero por internet y el transporte
                            del mismo a cualquier lugar de Colombia
                        </div>
                    </div>
                </div>
            </section>

            <div class="row logo">
                <div class="medium-6 columns">
                    <div id="logo">
                        <img src="{{ asset('imagenes/logo.png') }}" alt="Encarguelo.com" />
                    </div>
                </div>
                <div class="medium-6 columns">
                    <nav>
                        <ul>
                            <li><a href="http://encarguelo.com/">Home</a></li>
                            <li><a href="http://encarguelo.com/como-funciona/">&iquest;C&oacute;mo funciona?</a></li>
                            <li><a href="http://encarguelo.com/encargar/">Cotizaciones y tarifas</a></li>
                            <li><a href="http://encarguelo.com/contacto/">Contacto</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </header>
        <div class="row">
            <div class="small-12 columns">
                <div class="mensajes">
                    @if (Session::has('mensajeError'))
                    <div class="alert callout" data-closable>
                        {{ Session::get('mensajeError') }}
                        <button class="close-button" aria-label="Cerrar" type="button" data-close>
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    @if (Session::has('mensaje'))
                    <div class="success callout" data-closable>
                        {{ Session::get('mensaje') }}
                        <button class="close-button" aria-label="Cerrar" type="button" data-close>
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    @if (Session::has('mensajeAviso'))
                    <div class="warning callout" data-closable>
                        {{ Session::get('mensajeAviso') }}
                        <button class="close-button" aria-label="Cerrar" type="button" data-close>
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    @if (Session::has('mensajeExt'))
                    {{ Session::get('mensajeExt') }}
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="small-12 columns">
                @yield('content')
            </div>
        </div>
        <footer>
            <div class="row">
                <div class="small-12 columns">
                    <nav>
                        <ul>
                            <li><a href="http://encarguelo.com/contacto/">Contacto</a></li>
                            <li><a href="http://encarguelo.com/como-funciona/">&iquest;C&oacute;mo funciona?</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns">
                    Copyright 2015 &copy; <strong>Encarguelo.com</strong>
                </div>
            </div>
        </footer>
    </body>
</html>
