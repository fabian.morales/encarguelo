@extends('emails')

@section('content')
    <img src="{{ asset('imagenes/logo.png') }}" />

    <p>Apreciado/a {{ $pedido->cliente->nombres }}</p>

    <p>Su compra ya fue realizada y la estar&aacute; recibiendo en 10-15 d&iacute;as h&aacute;biles.  
        A continuaci&oacute;n la relaci&oacute;n de su pedido y el saldo pendiente</p>

    <p><strong>Detalles</strong></p>
    <hr />
    <table style="border-top: 1px solid #333; border-right: 1px solid #333; width: 100%;" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th style="border-bottom: 1px solid #333; border-left: 1px solid #333; padding: 5px; font-weight: bold; background-color: #ddd;">Producto</th>
                <th style="border-bottom: 1px solid #333; border-left: 1px solid #333; padding: 5px; font-weight: bold; background-color: #ddd;">Enlace (URL)</th>
                <th style="border-bottom: 1px solid #333; border-left: 1px solid #333; padding: 5px; font-weight: bold; background-color: #ddd;">Valor cotizado</th>
            </tr>
        </thead>
        <tbody>
            @foreach($pedido->detalles as $d)
            <tr>
                <td style="border-bottom: 1px solid #333; border-left: 1px solid #333; padding: 5px;">{{ $d->nombre }}</td>
                <td style="border-bottom: 1px solid #333; border-left: 1px solid #333; padding: 5px;">
                    @if(!empty($d->link))<a href="{{ \App\Detalle::addhttp($d->link) }}" target="_blank">Ver</a>@else &nbsp; @endif
                </td>
                <td style="border-bottom: 1px solid #333; border-left: 1px solid #333; padding: 5px;">$ {{ $d->valor }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <br />
    <strong>Especificaciones del producto:</strong> {{ $pedido->observaciones }} <br />
@stop
