@extends('emails')

@section('content')
    <img src="{{ asset('imagenes/logo.png') }}" />
    <p>Estimado/a {{ $pedido->cliente->nombres }}:</p>
    <p>Compartimos su cotizaci&oacute;n No. {{ $pedido->id }}</p>
    <p style='text-align: center'>
        <a href='{{ url('cotizacion/pagar/'.$token->token) }}' target='_blank' style='background-color: #2199e8; color: #fff; padding: 10px 20px; line-height: 40px; border-radius: 50px; margin: 10px auto;'>Paga aqu&iacute;</a>
    </p>

    <p><strong>Detalles</strong></p>
    <hr />
    <table style="border-top: 1px solid #333; border-right: 1px solid #333; width: 100%;" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th style="border-bottom: 1px solid #333; border-left: 1px solid #333; padding: 5px; font-weight: bold; background-color: #ddd;">Producto</th>
                <th style="border-bottom: 1px solid #333; border-left: 1px solid #333; padding: 5px; font-weight: bold; background-color: #ddd;">Enlace (URL)</th>
                <th style="border-bottom: 1px solid #333; border-left: 1px solid #333; padding: 5px; font-weight: bold; background-color: #ddd;">Valor cotizado</th>
            </tr>
        </thead>
        <tbody>
            @foreach($pedido->detalles as $d)
            <tr>
                <td style="border-bottom: 1px solid #333; border-left: 1px solid #333; padding: 5px;">{{ $d->nombre }}</td>
                <td style="border-bottom: 1px solid #333; border-left: 1px solid #333; padding: 5px;">
                    @if(!empty($d->link))<a href="{{ \App\Detalle::addhttp($d->link) }}" target="_blank">Ver</a>@else &nbsp; @endif
                </td>
                <td style="border-bottom: 1px solid #333; border-left: 1px solid #333; padding: 5px;">$ {{ $d->valor }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <br />

    <strong>Especificaciones del producto:</strong> {{ $pedido->observaciones }} <br />

    <p>El total cotizado es de <big><strong>$ {{ $pedido->valor }}</strong></big></p>
    <p>Otras formas de pago:  Una vez realizado el pago env&iacute;enos el comprobante a info@encarguelo.com o pague con tarjeta en 
        línea y seremos notificados autom&aacute;ticamente.</p>

    <table style="border-top: 1px solid #333; border-right: 1px solid #333; width: 100%;" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th style="border-bottom: 1px solid #333; border-left: 1px solid #333; padding: 5px; font-weight: bold; background-color: #ddd;">Consigaci&oacute;n o transferencia</th>
                <th style="border-bottom: 1px solid #333; border-left: 1px solid #333; padding: 5px; font-weight: bold; background-color: #ddd;">Pago por Efecty</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="border-bottom: 1px solid #333; border-left: 1px solid #333; padding: 5px;">
                    Consignaci&oacute;n o transferencia  a la cuenta de ahorros de Bancolombia #946.879.571.26 a nombre de 
                    Lesly Saenz de Mozzo, c&eacute;dula 20470166 de Chia. 
                </td>
                <td style="border-bottom: 1px solid #333; border-left: 1px solid #333; padding: 5px;">
                    Enviar giro a nombre de Alejandro Muller, c&eacute;dula 80.665.236 en Bogot&aacute;.
                </td>
            </tr>
        </tbody>
    </table>

    <br />
    <p>Le recordamos leer toda la cotizaci&oacute;n, el precio incluye compra, env&iacute;o, seguros e impuestos hasta Bogot&aacute;, 
    no hay ning&uacute;n cobro adicional. El tiempo estimado de entrega es de 10 a 15 d&iacute;as h&aacute;biles y <a href="http://encarguelo.com" target="_blank">Encarguelo.com</a> 
    responder&aacute; cualquier duda que tenga durante el proceso de compra. Algunas p&aacute;ginas se pueden demorar hasta 
    1 mes en hacer el env&iacute;o a nuestra bodega, as&iacute; que por favor tener en cuenta el tiempo de entrega del almac&eacute;n.</p>

    <p>NUESTRO SERVICIO INCLUYE TODO EL PROCESO DESDE LA COMPRA HASTA LA IMPORTACI&Oacute;N A COLOMBIA O &Uacute;NICAMENTE EL 
    ENV&Iacute;O DESDE ESTADOS UNIDOS. EL VALOR DEL SERVICIO ES NETO DESPU&Eacute;S DE RETENCIONES EN CASO DE QUE APLIQUEN.
    Total Compra + servicios + env&iacute;os y seguros + env&iacute;o en Bogot&aacute; EN PESOS. Los env&iacute;os a otras ciudades se 
    realiza contra-entrega por Coordinadora desde Bogot&aacute;.</p>

    <p><span style="color: #f00;">M&Iacute;NIMO EL 50% del VALOR DEBE SER CANCELADO PARA QUE SE EFECT&Uacute;E LA COMPRA. 
    UNA VEZ REALIZADO SU PAGO, ENV&Iacute;ENOS EL COMPROBANTE DE TRANSFERENCIA O CONSIGNACI&Oacute;N A INFO@ENCARGUELO.COM.</span>
    LE GARANTIZAMOS QUE SU PEDIDO SER&Aacute; ENTREGADO O SE LE DEVOLVER&Aacute; EL 100% DEL DINERO.</p>

    <p><strong>TIEMPO DE ENTREGA</strong></p>
    <p>10-15 d&iacute;as h&aacute;biles a partir de la fecha de compra.</p>

    <p>Para consignaciones nacionales el comprador deber&aacute; asumir los $9.800 de recargo que cobra el banco. 
    Para evitar el recargo puede hacer consignaci&oacute;n en un corresponsal no bancario o transferencia por 
    internet o por cajero.</p>

    <p><strong>PARA REALIZAR USTED LA COMPRA Y ENVIARLA A NUESTRA DIRECCI&Oacute;N</strong> puede enviarla a la siguiente direcci&oacute;n:</p>
    <p>Ysolina Montiel<br />
    18422 Via Di Sorrento<br />
    Boca Raton, Florida<br />
    Zip Code: 33496<br />
    Tel: 561-254-0634</p>

    <p>Una vez realice la compra, env&iacute;enos la factura y el n&uacute;mero de gu&iacute;a que le env&iacute;e la p&aacute;gina.</p>

    <p>Una vez haga la consignaci&oacute;n, env&iacute;enos el comprobante a info@encarguelo.com para proceder con la compra.</p>

    <p>Una vez hecha la compra, le enviamos la factura a su correo.</p>

    <p>Para cualquier inquietud, por favor cont&aacute;ctenos en info@encarguelo.com.</p>

    <p>En caso de hacer el pago 50-50, el comprador debe cancelar el 50% restante en un m&aacute;ximo de 10 d&iacute;as h&aacute;biles 
    despu&eacute;s de ser avisado que la mercanc&iacute;a est&eacute; en Colombia. De lo contrario ser&aacute; devuelta y no se devolver&aacute; 
    el pago ya efectuado.</p>

    <p>(<a href="http://encarguelo.com" target="_blank">Encarguelo.com</a> no se hace responsable por demoras por parte del almacen que vende el producto, retrasos 
    por parte de las autoridades, ni por otros retrasos inesperados) <span style="color: #f00;">Algunas p&aacute;ginas se pueden demorar hasta 
    1 mes en hacer el env&iacute;o a nuestra bodega as&iacute; que por favor tener en cuenta el tiempo de entrega del almac&eacute;n.</span></p>

    <p>Los env&iacute;os mayores a 20 libras ser&aacute;n enviados contra-entrega a cualquier ciudad a menos que sea pagado por 
    anticipado y se de la direcci&oacute;n de destino final para enviarlo directamente desde EEUU.</p>

    <p>En caso de hacer efectiva una garant&iacute;a o de devoluci&oacute;n por error en el env&iacute;o por parte del almac&eacute;n, 
    <a href="http://encarguelo.com" target="_blank">Encarguelo.com</a> presta el servicio de devoluci&oacute;n. Los gastos de env&iacute;o son responsabilidad del cliente.</p>

    <p>Cualquier devoluci&oacute;n debe ser hecha en la primera semana despu&eacute;s de ser entregada.</p>

    <p><a href="http://encarguelo.com" target="_blank">Encarguelo.com</a> le env&iacute;a su factura de compra. Por favor revise que en caso de que la mercanc&iacute;a llegue defectuosa, 
    se apelar&aacute; a Servientrega para el seguro y la mercanc&iacute;a ser&aacute; devuelta a cargo del comprador.</p>

    <p><a href="http://encarguelo.com" target="_blank">Encarguelo.com</a> es un intermediario que facilita la compra y el env&iacute;o del exterior. No somos la empresa de env&iacute;o.</p>

    <p>Esta cotizaci&oacute;n es v&aacute;lida hasta que la tienda cambie el precio del producto o hasta que el d&oacute;lar cambie de valor.</p>

    <p>Al recibir esta cotizaci&oacute;n ud. acepta recibir correos promocionales de <a href="http://encarguelo.com" target="_blank">Encarguelo.com</a>.</p>
@stop