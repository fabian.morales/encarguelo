@extends('emails')

@section('content')
    <img src="{{ asset('imagenes/logo.png') }}" />
    <p>       
        Apreciado {{ $cliente->nombres }}, <br />

        Hemos recibido tu solicitud.  Tu cotizaci&oacute;n estar&aacute; lista en m&aacute;ximo 24 horas h&aacute;biles.  Cualquier duda, 
        puedes llamarnos al 304-400-0742 o escribirnos a <strong>info@encarguelo.com</strong>
    </p>

    <h3>Detalles</h3>
    <hr />
    <table style="border-top: 1px solid #333; border-right: 1px solid #333; width: 100%;" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th style="border-bottom: 1px solid #333; border-left: 1px solid #333; padding: 5px; font-weight: bold; background-color: #ddd;">Producto</th>
                <th style="border-bottom: 1px solid #333; border-left: 1px solid #333; padding: 5px; font-weight: bold; background-color: #ddd;">Enlace (URL)</th>
            </tr>
        </thead>
        <tbody>
            @foreach($pedido->detalles as $d)
            <tr>
                <td style="border-bottom: 1px solid #333; border-left: 1px solid #333; padding: 5px;">{{ $d->nombre }}</td>
                <td style="border-bottom: 1px solid #333; border-left: 1px solid #333; padding: 5px;">
                    @if(!empty($d->link))<a href="{{ \App\Detalle::addhttp($d->link) }}" target="_blank">Ver</a>@else &nbsp; @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <br />
    <strong>Especificaciones del producto:</strong> {{ $pedido->observaciones }} <br />
    <p>Gracias por escoger <a href="http://encarguelo.com" target="_blank">Encarguelo.com</a></p>
@stop
