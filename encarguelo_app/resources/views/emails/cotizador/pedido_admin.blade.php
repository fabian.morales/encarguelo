@extends('emails')

@section('content')
    <img src="{{ asset('imagenes/logo.png') }}" />
    <h2>Pedido recibido</h2>

    <p>
        Se ha recibido un pedido con los siguientes detalles:
    </p>

    <p>
        <strong>Cliente:</strong> {{ $cliente->nombres }} {{ $cliente->apellidos }}<br />
        @if(!empty($cliente->telefono))
        <strong>Telefono:</strong> {{ $cliente->telefono }} <br />
        @endif
        <strong>Correo:</strong> {{ $cliente->email }} <br />
        <strong>Telefono:</strong> {{ $cliente->telefono }} <br />
        <strong>Fecha del pedido:</strong> {{ $pedido->fecha_creacion }} <br />
    </p>

    <h3>Detalles</h3>
    <hr />
    <table style="border-top: 1px solid #333; border-right: 1px solid #333; width: 100%;" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th style="border-bottom: 1px solid #333; border-left: 1px solid #333; padding: 5px; font-weight: bold; background-color: #ddd;">Producto</th>
                <th style="border-bottom: 1px solid #333; border-left: 1px solid #333; padding: 5px; font-weight: bold; background-color: #ddd;">Enlace (URL)</th>
            </tr>
        </thead>
        <tbody>
            @foreach($pedido->detalles as $d)
            <tr>
                <td style="border-bottom: 1px solid #333; border-left: 1px solid #333; padding: 5px;">{{ $d->nombre }}</td>
                <td style="border-bottom: 1px solid #333; border-left: 1px solid #333; padding: 5px;">
                    @if(!empty($d->link))<a href="{{ \App\Detalle::addhttp($d->link) }}" target="_blank">Ver</a>@else &nbsp; @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <br />
    <strong>Especificaciones del producto:</strong> {{ $pedido->observaciones }} <br />
@stop