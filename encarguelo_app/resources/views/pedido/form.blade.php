@extends('admin')

@section('js_header')
<script>
    (function (window, $){
        $(document).ready(function() {
            $("input[rel=detalle]").change(function() {
                var $valor = 0;
                $("input[rel=detalle]").each(function(i, o) {
                    $valor += parseFloat($(o).val());
                });
                
                $("#valor_total_span").html('$ ' + $valor);
                $("#valor_total").val($valor);
            });
        });
    })(window, jQuery);
</script>
@stop

@section('content')
<div class="row">
    <div class="small-12 columns">
        <h3 class="titulo seccion"><span>Datos del pedido</span></h3>
    </div>
</div>
<form id="form_pedido" name="form_pedido" action="{{ url('administrador/pedidos/guardar') }}" method="post">
    <input type="hidden" id="id" name="id" value="{{ $pedido->id }}" />
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <input type="hidden" name="valor_total" value="{{ $pedido->valor }}">
    
    <div class="row">
        <div class="medium-2 columns"><strong>Pedido:</strong></div>
        <div class="medium-10 columns">{{ $pedido->id }}</div>
        
        <div class="medium-2 columns"><strong>Fecha:</strong></div>
        <div class="medium-10 columns">{{ $pedido->fecha_creacion }}</div>
        
        <div class="medium-2 columns"><strong>Nombre Cliente:</strong></div>
        <div class="medium-10 columns">{{ $pedido->cliente->nombres }}</div>
        
        <div class="medium-2 columns"><strong>Apellido Cliente:</strong></div>
        <div class="medium-10 columns">{{ $pedido->cliente->apellidos }}</div>
        
        <div class="medium-2 columns"><strong>Correo:</strong></div>
        <div class="medium-10 columns">{{ $pedido->cliente->email }}</div>
        
        <div class="medium-2 columns"><strong>Tel&eacute;fono:</strong></div>
        <div class="medium-10 columns">@if(!empty($pedido->cliente->telefono)){{ $pedido->cliente->telefono }}@else &nbsp; @endif</div>
                
        <div class="medium-2 columns"><strong>Valor total:</strong></div>
        <div class="medium-10 columns"><span id='valor_total_span'>$ {{ $pedido->valor }}</span></div>
    </div>
    
    <div class="row separador">
        <div class="small-12 columns">
            <h3 class="titulo seccion"><span>Detalles</span></h3>
        </div>
    </div>
    
    <div class="row item lista head">
        <div class="small-4 columns"><strong>Nombre</strong></div>
        <div class="small-3 columns"><strong>Url</strong></div>
        <div class="small-5 columns"><strong>Valor</strong></div>
    </div>
    @foreach($pedido->detalles as $d)
    <div class="row item lista">
        <div class="small-4 columns">{{ $d->nombre }}</div>
        <div class="small-3 columns">@if(!empty($d->link))<a href="{{ \App\Detalle::addhttp($d->link) }}" target="_blank"><i class="fi-web"></i> Ver</a>@else &nbsp; @endif</div>
        <div class="small-5 columns"><input rel="detalle" type="text" name="detalle[{{ $d->id }}]" id="detalle_{{ $d->id }}" value="{{ $d->valor }}" /></div>
    </div>
    @endforeach
    <div class="row separador"></div>
    
    <div class="row">
        <div class="medium-2 columns"><strong>Observaciones:</strong></div>
        <div class="medium-10 columns">{{ $pedido->observaciones }}</div>
    </div>
    
    <div class="row separador"></div>
    <div class="row">
        <div class="small-12 columns text-right">
            <a class="button alert" href="{{ url('/administrador/pedidos/') }}" />Cancelar</a>
            @if ($pedido->estado == 'N' || $pedido->estado == 'C')
            <input type="submit" value="Guardar" class="button default" />
            @endif
        </div>
    </div>
</form>
@stop