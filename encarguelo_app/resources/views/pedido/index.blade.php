@extends('admin')

@section('js_header')
<script>
(function($, window){
    $(document).ready(function() {
        var $datePickerOpc = {
            dateFormat: "yy-mm-dd",
            dayNames: [ "Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado" ],
            dayNamesMin: [ "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa" ],
            dayNamesShort: [ "Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb" ],
            monthNames: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
            monthNamesShort: [ "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic" ],
            currentText: "Hoy",
            closeText: "Aceptar",
            showButtonPanel: true
        };
        
        $("#fecha_inicio, #fecha_fin").datepicker($datePickerOpc);
        
        $("#btnLimpiar").click(function(e) {
            e.preventDefault();
            $("#fecha_inicio").val('');
            $("#fecha_fin").val('');
            $("#cliente").val('');
        });
        
        $("#lnkFiltro").click(function(e) {
            e.preventDefault();
            $("#form_filtro").toggle("slow", function() {
                if ($("#form_filtro").is(":visible")){
                    $("#lnkFiltro i").removeClass("fi-arrow-down").addClass("fi-arrow-up");
                }
                else{
                    $("#lnkFiltro i").removeClass("fi-arrow-up").addClass("fi-arrow-down");
                }
            });
        });
        
        $("a[rel=borrar]").click(function(e) {
            if (!confirm('¿Desea borrar este pedido?')){
                e.preventDefault();
                return;
            }
        });
    });
})(jQuery, window);
</script>
@stop

@section('content')
<div class="row">
    <div class="small-12 columns"><a href="{{ url('/administrador/pedidos/clientes') }}"><i class="fi-torso"></i> Descargar listado de clientes</a></div>
</div>
<div class="row titulo lista">
    <div class="small-12 columns">
        Filtro 
        <a id="lnkFiltro" href="#">
            @if (!empty($fecha_inicio.$fecha_fin.$cliente))
            <i class="fi-arrow-up">&nbsp;</i>
            @else
            <i class="fi-arrow-down">&nbsp;</i>
            @endif
        </a>
    </div>
</div>

<form id="form_filtro" method='post' action='{{ url('/administrador/pedidos') }}' @if (empty($fecha_inicio.$fecha_fin.$cliente)) style="display: none" @endif>
    <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
    <div class="row">
        <div class="medium-4 columns">
            <div>Fecha inicio</div>
            <input type="text" name="fecha_inicio" id="fecha_inicio" value='{{ $fecha_inicio }}' />
        </div>
        <div class="medium-4 columns">
            <div>Fecha fin</div>
            <input type="text" name="fecha_fin" id="fecha_fin" value='{{ $fecha_fin }}' />
        </div>
        <div class="medium-4 columns">
            <div>Cliente</div>
            <input type="text" name="cliente" id="cliente" value='{{ $cliente }}' />
        </div>
        <div class="medium-12 columns text-right">
            <input type="button" value="Limpiar" class="tiny button warning" id='btnLimpiar' />
            <input type="submit" value="Filtrar" class="tiny button default" />
        </div>
    </div>
    <hr />
</form>
<br />
@include('pedido.lista', array("pedidos" => $pedidos))
@stop