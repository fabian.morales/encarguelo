<div class="row">
    <div class="small-12 columns">
        <h3 class="titulo seccion"><span>Intentos de pago pedido #{{ $pedido->id }}</span></h3>
        <table>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Fecha</th>
                    <th>Franquicia</th>
                    <th>Estado</th>
                    <th>N&uacute;m. transacci&oacute;n</th>
                    <th>Aprobaci&oacute;n</th>
                    <th>Mensaje</th>
                    <th>Token</th>
                <tr>
            </thead>
            <tbody>
                @forelse($pedido->intentosPago as $i)
                <tr>
                    <td>{{ $i->id }}</td>
                    <td>{{ $i->fecha_transaccion }}</td>
                    <td>{{ $i->franquicia }}</td>
                    <td>{{ $estados[$i->estado] }}</td>
                    <td>{{ $i->num_transaccion }}</td>
                    <td>{{ $i->cod_aprobacion }}</td>
                    <td>{{ $i->mensaje }}</td>
                    <td>{{ $i->token->token }}</td>
                <tr>
                    @empty
                <tr>
                    <td colspan="8">
                        <div class="text-center"><strong>No hay intentos de pago para este pedido</strong></div>
                    </td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>