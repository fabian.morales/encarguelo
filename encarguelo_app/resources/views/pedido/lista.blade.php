<div class="row titulo lista">
    <div class="small-12 columns">Lista de pedidos</div>
</div>
<div class="row item lista head">
    <div class="small-2 columns">N&uacute;m</div>
    <div class="small-3 columns">Cliente</div>
    <div class="small-3 columns">Fecha</div>
    <div class="small-2 columns">Estado</div>
    <div class="small-2 columns">Acciones</div>
</div>
@forelse($pedidos as $p)
<div class="row item lista">
    <div class="small-2 columns">{{ $p->id }}</div>
    <div class="small-3 columns">{{ $p->cliente->nombres }} {{ $p->cliente->apellidos }}</div>
    <div class="small-3 columns">{{ $p->fecha_creacion }}</div>
    <div class="small-2 columns">{{ $estados[$p->estado] }}</div>
    <div class="small-2 columns">
        @if ($p->estado == 'N' || $p->estado == 'C')
        <a data-tooltip aria-haspopup="true" class="has-tip right" data-disable-hover="false" tabindex="1" href="{{ url('administrador/pedidos/editar/'.$p->id) }}" title='Editar'><i class="fi-pencil"></i>&nbsp;</a>
        @endif
        @if ($p->estado != 'N' && $p->estado != 'C')
        <a data-tooltip aria-haspopup="true" class="has-tip right" data-disable-hover="false" tabindex="1" href="{{ url('administrador/pedidos/editar/'.$p->id) }}" title='ver pedido'><i class="fi-book"></i>&nbsp;</a>
        @endif
        @if ($p->estado == 'C')
        <a data-tooltip aria-haspopup="true" class="has-tip right" data-disable-hover="false" tabindex="1" href="{{ url('administrador/pedidos/mail/'.$p->id) }}" title='Enviar correo al cliente'><i class="fi-mail"></i>&nbsp;</a>
        <a data-tooltip aria-haspopup="true" class="has-tip right" data-disable-hover="false" tabindex="1" href="{{ url('administrador/pedidos/pagar/'.$p->id) }}" title='Marcar como pagado'><i class="fi-dollar-bill"></i>&nbsp;</a>
        @endif
        @if ($p->estado == 'P')
        <a data-tooltip aria-haspopup="true" class="has-tip right" data-disable-hover="false" tabindex="1" href="{{ url('administrador/pedidos/enviar/'.$p->id) }}" title='Marcar como enviado'><i class="fi-map"></i>&nbsp;</a>
        @endif
        @if ($p->estado != 'N')
        <a data-tooltip aria-haspopup="true" class="has-tip right" data-disable-hover="false" tabindex="1" href="{{ url('administrador/pedidos/pagos/'.$p->id) }}" title='Ver intentos de pago' data-featherlight><i class="fi-dollar"></i>&nbsp;</a>
        @endif
        
        <a rel="borrar" data-tooltip aria-haspopup="true" class="has-tip right" data-disable-hover="false" tabindex="1" href="{{ url('administrador/pedidos/borrar/'.$p->id) }}" title='Borrar pedido'><i class="fi-trash"></i>&nbsp;</a>
    </div>
</div>
@empty
<div class="row">
    <div class="small-12 columns text-center">
        <br />
        <p><strong>No se encontraron pedidos</strong></p>
    </div>
</div>
@endforelse
<div class="row">
    <div class="small-12 columns text-center">
        {!! $pedidos->render() !!}
    </div>
</div>