@extends('admin')

@section('content')
<form id="form_login" name="form_login" action="{{ url('/administrador/login') }}" method="post">
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <div class="row">
        <div class="large-5 medium-6 small-12 columns large-centered medium-centered cont_form">
            <fieldset class="fieldset">
                <legend>Inicio de sesi&oacute;n</legend>
                <div class="row">
                    <div class="small-12 columns">
                        <div class="input-group">
                            <span class="input-group-label"><i class="fi-torso"></i></span>
                            <input class="input-group-field" type="text" name="login" id="login" placeholder="Nombre de usuario" />    
                        </div>

                        <div class="input-group">
                            <span class="input-group-label"><i class="fi-lock"></i></span>
                            <input class="input-group-field" type="password" name="password" id="password" placeholder="Clave" />
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="small-12 columns">
                        <input type="submit" value="Iniciar sesi&oacute;n" class="button medium default float-right" />
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
</form>
@stop