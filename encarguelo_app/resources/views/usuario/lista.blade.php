<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('administrador/usuarios/crear') }}" class="button alert">Usuario nuevo <i class="fi-plus"></i></a>
    </div>
</div>
<div class="row titulo lista">
    <div class="small-12 columns">Lista de usuarios</div>
</div>
<div class="row item lista head">
    <div class="small-2 columns">N&uacute;m</div>
    <div class="small-4 columns">Nombre</div>
    <div class="small-4 columns">Correo</div>
    <div class="small-2 columns">Editar</div>
</div>
@foreach($usuarios as $u)
<div class="row item lista">
    <div class="small-2 columns">{{ $u->id }}</div>
    <div class="small-4 columns">{{ $u->nombre }}</div>
    <div class="small-4 columns">{{ $u->email }}</div>
    <div class="small-2 columns"><a data-tooltip aria-haspopup="true" class="has-tip right" data-disable-hover="false" tabindex="1" title='Editar usuario' href="{{ url('administrador/usuarios/editar/'.$u->id) }}"><i class="fi-pencil"></i></a></div>
</div>
@endforeach
<div class="row">
    <div class="small-12 columns text-center">
        {!! $usuarios->render() !!}
    </div>
</div>